
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/equiplist', component: () => import('pages/equipment.vue') },
      { path: '/', component: () => import('components/dashboard.vue') },
      { path: '/userprofile', component: () => import('pages/UserProfile.vue') },
      { path: '/editprofile', component: () => import('pages/editProfile.vue') },
      { path: '/view', component: () => import('pages/viewEquipment.vue') },
      { path: '/add-phone', component: () => import('pages/addphone.vue') },
      { path: '/add-laptop', component: () => import('pages/addlaptop.vue') },
      { path: '/add-desktop', component: () => import('pages/adddesktop.vue') },
      { path: '/add-other', component: () => import('pages/addother.vue') },
      { path: '/getchuchu', component: () => import('pages/exampleComponent.vue') }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: '/login', component: () => import('pages/login.vue') },
      { path: '/register', component: () => import('pages/register.vue') },
      { path: '/forgot', component: () => import('pages/ForgotPass.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
